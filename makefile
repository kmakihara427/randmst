# defined makefile variable
JCC = javac

# defines flags, compiles with debugging info
JFLAGS = -g

# default make
default: randmst.class Graph.class UnionFind.class Edge.class Vertex.class

# This target entry builds randmst.class, which depends on randmst.java

randmst.class: randmst.java
		$(JCC) $(JFLAGS) randmst.java

Graph.class: Graph.java
		$(JCC) $(JFLAGS) Graph.java

UnionFind.class: UnionFind.java
		$(JCC) $(JFLAGS) UnionFind.java

Edge.class: Edge.java
		$(JCC) $(JFLAGS) Edge.java

Vertex.class: Vertex.java
		$(JCC) $(JFLAGS) Vertex.java

# To start over from scratch, type make clean which gets rid of .class files and rebuilds them
clean:
		$(RM) *.class