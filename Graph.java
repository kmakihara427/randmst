import java.util.*;
import java.math.*;

public class Graph {
	public final int vertexCount;
	public final int edgeCount;
	public Vertex[] vertices;
	//public Edge[] edges;
	public ArrayList<Edge> edges;

	public Graph(int vertexCount) {
		this.vertexCount = vertexCount;
		this.vertices = new Vertex[vertexCount];
		this.edgeCount = this.calculateEdgeCount().intValue();
		//this.edges = new Edge[edgeCount];
		this.edges = new ArrayList<Edge>();
	}

	private BigInteger calculateEdgeCount() {
		BigInteger bigV = BigInteger.valueOf(vertexCount);
		bigV = bigV.multiply(bigV.subtract(BigInteger.valueOf(1)));
		return bigV.divide(BigInteger.valueOf(2));
	}

	public void sort() {
		Collections.sort(edges);
	}

	private void generateVertices(int dimension) {
		Random rand = new Random();

		for (int i = 0; i < this.vertexCount; i++) {
			Vertex vertex = new Vertex(dimension, rand);
			this.vertices[i] = vertex;
		};
	}

	public void makeGraph(int dimension) {
		int counter = 0;
		Random rand = new Random();
		this.generateVertices(dimension);
		float weight;
		float averageWeight;
		switch (dimension) {
			case 0: averageWeight = 0.924f * (float) Math.pow((double) this.vertexCount, -0.99);
			break;
			case 2: averageWeight = 0.7056f * (float) Math.pow((double) this.vertexCount, -0.509);
			break;
			case 3: averageWeight = 0.8155f * (float) Math.pow((double) this.vertexCount, -0.36);
			break;
			case 4: averageWeight = 0.8243f * (float) Math.pow((double) this.vertexCount, -0.268);
			break;
			default: averageWeight = 0.0f;
			break;
		}
		float maxWeight = 3.05f * averageWeight;
		for (int i = 0; i < vertexCount; i++) {
			for (int j = i + 1; j < vertexCount; j++) {
				if (dimension == 0) {
					weight = rand.nextFloat();
				} else {
					weight = this.vertices[i].distanceTo(this.vertices[j]);
				}
				if (weight > maxWeight) {
					continue;
				}
				Edge edge = new Edge(i, j, weight);
				//this.edges[counter] = edge;
				this.edges.add(counter, edge);
				//System.out.println(this.vertices[i].ordinates + " is connected to " + this.vertices[j].ordinates + " with an edge of weight: " + weight);
				counter++;
			}
			//this.vertices[i] = null;
		}
	}
}