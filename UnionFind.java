public class UnionFind {
	public int[] parent;
	public int[] rank;

	public UnionFind(int vertexCount){
		parent = new int[vertexCount];
		rank = new int[vertexCount];
		for (int i = 0; i < vertexCount; i++) {
			parent[i] = i;
			rank[i] = 0;
		}
	}
	
	public void union(int x, int y) {
		int root_x = find(x);
		int root_y = find(y);
        int rank_x = rank[x];
        int rank_y = rank[y];
		if (rank_x == rank_y) {
			parent[root_x] = root_y;
			rank[y] ++;
		}
		else if (rank_x > rank_y){
			parent[root_y] = root_x;
		}
		else{
			parent[root_x] = root_y;
		}
	}

	public int find(int vertex) {
		if (vertex != parent[vertex]) {
			parent[vertex] = find(parent[vertex]);
			return parent[vertex];
			//return find(parent[vertex]);
		} else {
			return vertex;
		}
	}

	public void print() {
		for (int i = 0; i < parent.length; i++) {
			System.out.print(i + ".  rank:" + rank[i] + " parent:" + parent[i]);
			System.out.print("\n");
		}
	}
}
