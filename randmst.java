import java.util.*;
import java.lang.*;
import java.math.*;

public class randmst {

	public float average;

	public static void main(String[] args) {
		int numVertices = Integer.parseInt(args[1]);
		int numTrials = Integer.parseInt(args[2]);
		int dimension = Integer.parseInt(args[3]);

		randmst randmst = new randmst();
		randmst randmst2 = new randmst();

		float average = 0.0f;

		if (dimension >= 0 && dimension < 5 && dimension != 1) {
			//graph.makeGraph(dimension);

			for (int i = 0; i < numTrials; i++) {
				Graph graph = new Graph(numVertices);
				UnionFind uf = new UnionFind(numVertices);

				graph.makeGraph(dimension);

				average += randmst.averageWeight(graph, uf);
			}
			float averageOfaverages = average/((float) numTrials);
			System.out.println(averageOfaverages + " " + numVertices + " " + numTrials + " " + dimension);
		} else {
			System.out.println("Dimension is not supported");
		}

		//Get the jvm heap size.
		long heapSize = Runtime.getRuntime().totalMemory();

		//Print the jvm heap size.
		System.out.println("Heap Size = " + heapSize);
	}

	public ArrayList<Edge> kruskals(Graph graph, UnionFind uf) {
		ArrayList<Edge> x = new ArrayList<Edge>(graph.vertexCount-1);
		graph.sort();
		int counter = 0;

		for (int k = 0; k < graph.edges.size(); k++) {
			Edge edge = graph.edges.get(k);

			float weight = edge.weight;
			int i = edge.getI();
			int j = edge.getJ();

			assert i < graph.vertexCount;
			assert j < graph.vertexCount;

			//System.out.println("i: " + i + " j: " + j);

			if (uf.find(i) != uf.find(j)) {
				x.add(counter,edge);
				uf.union(i, j);
				//System.out.println(weight + " connects " + i + " and " + j);
				counter++;
			}
		}
		return x;
	}

	public float averageWeight(Graph graph, UnionFind uf) {
		randmst randmst = new randmst();
		ArrayList<Edge> results = randmst.kruskals(graph, uf);
		float sum = 0f;
		for (int i = 0;i < results.size(); i++) {
			sum += results.get(i).weight;
		}
		//System.out.println(" average is: " + sum/results.size());
		return sum;
	}
}