import java.util.*;
import java.math.*;

public class Vertex {
	public ArrayList<Float> ordinates;

	public Vertex(int dimension, Random rand) {
		this.ordinates = new ArrayList<Float>();
		int numOrdinates;
		if (dimension == 0) {
			numOrdinates = 1;
		} else {
			numOrdinates = dimension;
		}
		for (int i = 0; i < numOrdinates; i++) {
			this.ordinates.add(rand.nextFloat());
		}
	}

	public float distanceTo(Vertex other) {
		float sum = 0;

		for (int i = 0; i < this.ordinates.size(); i++) {
			sum += (ordinates.get(i) - other.ordinates.get(i)) * (ordinates.get(i) - other.ordinates.get(i));
		}

		return (float) Math.sqrt(sum);
	}
}