import java.util.*;
import java.math.*;

public class Edge implements Comparable<Edge> {
  	public final int i;  
  	public final int j;
  	public final float weight; 
  	//private static int MASK = 0xffff;

  	public Edge(int i, int j, float weight) { 
    	//this.ij = (i << 16) + j; 
    	this.i = i;
    	this.j = j;
    	this.weight = weight;
	} 

	public int getI() {
		//return this.ij >> 16;
		return this.i;
	}

	public int getJ() {
		//return this.ij & MASK;
		return this.j;
	}

	@Override
	public int compareTo(Edge other) {
		return Float.compare(this.weight, other.weight);
	}
} 
